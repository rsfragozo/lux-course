interface Square {
}

class Mine implements Square {
}

class ValueSquare implements Square{
    -int value
    +int getValue()
    +void setValue()
}

class FlagSquare implements Square {
}

class FlyweightFactory {
    -Map<Square, Image> flyweightMap
    +Image getFlyweight()
    +void addFlyweight()
}