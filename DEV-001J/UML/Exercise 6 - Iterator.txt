interface Iterator {
    +boolean hasNext()
    +Object next()
}

interface Container {
    +Iterator getIterator()
}

interface FSItem {
    +String getName()
    +void setName()
    +String getPath()
    +FSItem getParent()
}

class Directory implements Container

class Directory implements FSItem {
    -List<FSItem> children
    -String name
    -boolean fileSystem
    
    +List<FSItem> getChildren()
    +boolean isFileSystem()
    +String getName()
    +void setName()
    +String getPath()
    +FSItem getParent()
    +Iterator getIterator()
}

class File implements FSItem {
    -String name
    
    +String getName()
    +void setName()
    +String getPath()
    +FSItem getParent()
}

class Link implements FSItem {
    -String name
    -FSItem link
    
    +FSItem getLink()
    +String getName()
    +void setName()
    +String getPath()
    +FSItem getParent()
}

