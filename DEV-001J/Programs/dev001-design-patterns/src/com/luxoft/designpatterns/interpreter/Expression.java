package com.luxoft.designpatterns.interpreter;

public interface Expression {
	public boolean interpret(String context);
}
