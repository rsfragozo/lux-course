package com.luxoft.designpatterns.command;

public interface Command {
	void execute();
    void printCommandInfo();
}
