package com.luxoft.designpatterns.singleton;

import com.luxoft.designpatterns.singleton.staticfactory.Bank;

public class Main {

    public static void main(String[] args) {
        Bank bank = Bank.getInstance();

        System.out.println(bank);

        bank = Bank.getInstance();
        System.out.println(bank);
    }
}
