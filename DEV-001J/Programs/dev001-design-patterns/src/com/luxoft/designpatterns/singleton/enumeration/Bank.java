package com.luxoft.designpatterns.singleton.enumeration;

//Enum singleton - the preferred approach
public enum Bank {
	INSTANCE;
}

