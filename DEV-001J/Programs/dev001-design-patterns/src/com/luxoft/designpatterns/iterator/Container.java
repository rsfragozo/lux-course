package com.luxoft.designpatterns.iterator;

public interface Container {
	public Iterator getIterator();
}
