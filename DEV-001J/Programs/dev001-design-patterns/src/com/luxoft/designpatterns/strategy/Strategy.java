package com.luxoft.designpatterns.strategy;

public interface Strategy {
	public boolean acceptClient(Client client);
}
