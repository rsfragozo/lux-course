package com.luxoft.designpatterns.adapter;

public interface Client {
	
	public String greet();

}
