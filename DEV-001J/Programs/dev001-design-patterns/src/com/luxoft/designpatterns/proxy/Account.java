package com.luxoft.designpatterns.proxy;

public interface Account {
	double getBalance();
	void verifyAccount();
}
