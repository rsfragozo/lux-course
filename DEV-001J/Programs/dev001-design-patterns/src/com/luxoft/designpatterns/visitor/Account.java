package com.luxoft.designpatterns.visitor;

import com.luxoft.designpatterns.visitor.ComponentVisitor;

public interface Account {
	void withdraw(double amount) throws NotEnoughFundsException;
	double getBalance();
	public void accept(ComponentVisitor componentVisitor);
}
